/*******************************************************************************
 * Copyright (c) 2010-2016, G. Weirich
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    G. Weirich - initial implementation
 *    G. Weirich - extract to randomizer
 *    
 *
 *    
 *******************************************************************************/
package ch.rgw.randomizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;

public class Namen {

	List<String> vornamen;
	List<String> nachnamen;

	public Namen() {
		try {
			InputStream iFnames = this.getClass().getResourceAsStream("vornamen.txt");
			vornamen = new LinkedList<String>();
			Reader infilevn = new InputStreamReader(iFnames);
			BufferedReader vn = new BufferedReader(infilevn);
			String line;
			while ((line = vn.readLine()) != null) {
				vornamen.add(line);
			}
			vn.close();

			nachnamen = new LinkedList<String>();
			InputStream iLnames = this.getClass().getResourceAsStream("nachnamen.txt");
			Reader infilenn = new InputStreamReader(iLnames);
			BufferedReader nn = new BufferedReader(infilenn);
			while ((line = nn.readLine()) != null) {
				nachnamen.add(line);
			}
			nn.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getRandomVorname() {
		return vornamen.get((int) Math.round(Math.random() * (vornamen.size() - 1))).trim();
	}

	public String getRandomNachname() {
		return nachnamen.get((int) Math.round(Math.random() * (nachnamen.size() - 1))).trim();
	}

	
}
