package ch.rgw.randomizer.test;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

import ch.rgw.randomizer.Generator;

public class Testit {

	@Test
	public void test() {
		Generator rnd=new Generator();
		assertTrue(rnd.getRandomFirstname().length()>0);
		assertTrue(rnd.getRandomLastname().length()>0);
		assertTrue(rnd.getRandomDate().before(new Date()));
		assertTrue(rnd.getRandomBirthdate().length()==8);
		assertTrue(rnd.getRandomStreet().length()>0);
		assertTrue(rnd.getRandomZip().length()>0);
		assertTrue(rnd.getLipsumParagraph().length()>0);
		assertTrue(rnd.getLipsumSentence().length()>0);
		assertTrue(rnd.getRandomCharSequence(10, 100).length()>=10);
		assertTrue(rnd.getRandomNumber(25, 30)>=25);
		
	}

}
