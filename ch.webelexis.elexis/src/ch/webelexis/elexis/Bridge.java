/**
 * Copyright (c) 2015 by G. Weirich
 * All rights reserved
 */
package ch.webelexis.elexis;

import ch.rgw.elexisrobot.ContactAccessor;
import ch.webelexis.Cleaner;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

public class Bridge extends AbstractVerticle {
	private ContactAccessor accessor;

	@Override
	public void start() throws Exception {
		// TODO Auto-generated method stub
		super.start();
		EventBus eb = vertx.eventBus();
		eb.consumer("ch.elexis.bridge.login", msg -> {
			Cleaner cl = new Cleaner(msg);
			accessor = new ContactAccessor(cl.get("username", Cleaner.NAME, false),
					cl.get("password", Cleaner.TEXT, false));
		});

		eb.consumer("ch.webelexis.elexis.bridge.kontakt", msg->{
		
			});
		eb.consumer("ch.webelexis.elexis.bridge", msg -> {
			JsonObject query = (JsonObject) msg.body();
			switch (query.getString("action")) {
			case "loadConsultations":

				break;
			case "loadDocument":
				break;
			case "saveKons":
				break;
			case "storeDocument":
				break;

			default:
				msg.reply(new JsonObject().put("result", "error").put("message", "bad action code"));
			}
		});
	}

}
