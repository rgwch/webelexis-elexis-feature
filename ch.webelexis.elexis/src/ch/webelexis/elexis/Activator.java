/**
 * Copyright (c) 2015 by G. Weirich
 * All rights reserved
 */

package ch.webelexis.elexis;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

public class Activator implements BundleActivator {

	private static BundleContext context;
	private Vertx vertx;
	private String deploymentID;

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.
	 * BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {

		Activator.context = bundleContext;
		VertxOptions options = new VertxOptions();
		Vertx.clusteredVertx(options, res -> {
			if (res.succeeded()) {
				vertx = res.result();
				vertx.deployVerticle(new Bridge(), dres -> {
					if (dres.succeeded()) {
						deploymentID = dres.result();
					} else {
						// ElexisStatus status=new
						// ElexisStatus(ElexisStatus.LOG_ERRORS,
						// "ch.webelexis.elexis", 1, "could not deploy
						// verticle", null);
						throw (new RuntimeException("could not launch Webelexis Verticle", dres.cause()));
					}
				});
			} else {
				throw new RuntimeException("could not launch Vertx", res.cause());
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
		if (vertx != null) {
			if (deploymentID != null) {
				vertx.undeploy(deploymentID);
			}
			vertx.close();
		}
	}

}
