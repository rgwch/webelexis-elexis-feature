package ch.rgw.elexisrobot.test;

import static org.junit.Assert.assertTrue;

import java.io.StringWriter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonWriter;

import org.junit.Test;

import ch.rgw.elexisrobot.ContactAccessor;
import junit.framework.Assert;

public class Robotest {

	@Test
	public void test() {
		ContactAccessor ca = new ContactAccessor("tester", "topsecret");
		JsonObject result = ca.find("meier", ContactAccessor.WITH_NAME | ContactAccessor.WITH_ADDRESS);
		assertTrue(result.getString("status").equals("ok"));
		
		StringWriter check = new StringWriter();
		JsonWriter writer = Json.createWriter(check);
		writer.writeObject(result);
		System.out.println(check.toString());
		Assert.assertNotNull(result);
	}

}
