package ch.rgw.elexisrobot;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

public class ExpiringHashMap {
	private HashMap<Object, entry> entries=new HashMap<>();
	private gc gcTask =new gc();
	private long graceTime=300000;
	
	public ExpiringHashMap(long graceTime){
		Timer timer=new Timer();
		this.graceTime=Math.max(60000,graceTime);
		timer.schedule(gcTask, 5000, 10000);
	}
	public void put(Object key,Object value){
		entries.put(key, new entry(value));
	}
	
	public Object get(Object key){
		entry ret=entries.get(key);
		if(ret!=null){
			return ret.val;
		}
		return null;
	}
	
	
	private class entry{
		entry(Object val){
			this.val=val;
			this.time=System.currentTimeMillis();
		}
		Object val;
		long	time;
	}
	private class gc extends TimerTask{

		@Override
		public void run() {
			Iterator<entry> it=entries.values().iterator();
			long cutoff=System.currentTimeMillis()-graceTime;
			while(it.hasNext()){
				entry e=it.next();
				if(e.time<cutoff){
					it.remove();
				}
			}
		}
		
	}
}
