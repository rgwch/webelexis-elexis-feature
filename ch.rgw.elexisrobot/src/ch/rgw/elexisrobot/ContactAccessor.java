package ch.rgw.elexisrobot;

import java.util.List;
import java.util.UUID;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ch.elexis.data.Kontakt;
import ch.elexis.data.Query;
import ch.rgw.randomizer.Generator;

public class ContactAccessor extends Accessor {
	/** add name, gender and birthdate to the result */
	public static final int WITH_NAME = 0x0001;
	/** add street, zip, place and phone# to the result */
	public static final int WITH_ADDRESS = 0x0002;
	/** add medication list to the result */
	public static final int WITH_MEDICATION = 0x0004;
	/** add insuranxe cases to the result */
	public static final int WITH_CASES = 0x0008;
	/** add consultations to the result */
	public static final int WITH_CONSULTATION = 0x0010;

	private Generator rnd = null;

	public ContactAccessor(String name, String pwd) {
		super(name, pwd);
	}

	public ContactAccessor(Object token) {
		super(token);
	}

	public JsonObject find(String pattern, int flags) {
		JsonArrayBuilder ret = Json.createArrayBuilder();
		JsonObjectBuilder result=Json.createObjectBuilder();
		switch (getRole()) {
		case "read":
		case "write":
			Query<Kontakt> qbe = new Query<Kontakt>(Kontakt.class);
			qbe.add(Kontakt.FLD_NAME1, Query.LIKE, pattern);
			qbe.or();
			qbe.add(Kontakt.FLD_NAME2, Query.LIKE, pattern);
			List<Kontakt> contacts = qbe.execute();
			result.add("status", "error").add("message", "not implemented");
			break;
		default:
			// Guest and unauthorized access: return 0 to 999 results with
			// garbage
			long resultCount = Math.round(1000 * Math.random());
			rnd = new Generator();
			for (int i = 0; i < resultCount; i++) {
				JsonObjectBuilder jb = Json.createObjectBuilder().add("guid", UUID.randomUUID().toString());
				if ((flags & WITH_NAME) != 0) {
					jb.add("lastname", rnd.getRandomLastname());
					jb.add("firstname", rnd.getRandomFirstname());
					jb.add("birthdate", rnd.getRandomBirthdate());
				}
				if ((flags & WITH_ADDRESS) != 0) {
					jb.add("street", rnd.getRandomStreet());
					jb.add("zip", rnd.getRandomZip());
					jb.add("place", rnd.getRandomCity());
					jb.add("phone1", rnd.getRandomPhoneNumber());
					jb.add("phone2", rnd.getRandomPhoneNumber());
				}
				if ((flags & WITH_CONSULTATION) != 0) {
					JsonArrayBuilder jaCons = Json.createArrayBuilder();
					int rounds = rnd.getRandomNumber(1, 30);
					for (int j = 0; j < rounds; j++) {
						jaCons.add(Json.createObjectBuilder().add("guid", UUID.randomUUID().toString())
								.add("date", rnd.getRandomDate().toString()).add("text", rnd.getLipsumParagraph())
								.add("case", UUID.randomUUID().toString()).build());
					}
					jb.add("consultations", jaCons.build());
				}
				JsonObject jo=jb.build();
				ret.add(jo);

			}
			result.add("status", "ok").add("count", resultCount);
		}
		
		result.add("values", ret.build());
		return result.build();
	}

}
