package ch.rgw.elexisrobot;

import java.util.UUID;

public class Accessor {
	protected Object uid;
	private static final ExpiringHashMap tokens=new ExpiringHashMap(300000);

	protected Accessor(String username, String Password){
		// todo: get matching role(s) for user
		uid= UUID.randomUUID();
		// give unauthorized users the role "guest"
		tokens.put(uid, "guest");
	}
	
	protected Accessor(Object token){
		uid=token;
	}
	
	protected String getRole(){
		Object ret= tokens.get(uid);
		if(ret==null){
			ret="guest";
		}
		return ret.toString();
	}
	
}
