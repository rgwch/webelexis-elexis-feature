# Elexisrobot

This plugin gives scriptable and headless access to some of the data types of elexis.

## Usage

    ContactAccessor cas=new ContactAccessor(username,password);
    JsonObject result=cas.find("m??er",WITH_NAME|WITH_ADDRESS);
    if(result.getString("status").equals("ok")){
	    JsonArray contacts=result.getJsonArray("values");
    }else{
	    System.out.print("oops: "+result.getString("message"));
    }

    
## Data formats

All returned data are Json Objects

### Contact

	{
		"guid": String,
		"firstname": String,
		"lastname": String,
		"birthdate": YYYYMMDD,
		"gender":	"m" or "f",
		"consultations": JsonArray,
		"medications": JsonArray,
		"problems": JsonArray,
		"allergies": JsonArray
	}

### Consultation

	{
		"guid": String,
		"problems": JsonArray
		"datetime": long,
		"lastedit": long,
		"responsible": guid,
		"editor": guid,
		"text": String
	}
	
### Medication
	
	{
		"guid": String,
		"problem": JsonObject,
		"product": String,
		"substances": JsonArray,
		"dosage": JsonObject,
		"unit": String,
		"remark"; String
	}

#### Substance

	{
		"guid": String,
		"atc": String,
		"name": String,
		"amount": float
	}
	
#### Dosage

	{
		"type": "fixed"|"optional",
		"early": float,
		"breakfast": float,
		"noon": float,
		"evening": float,
		"late": float,
		"up to": float	
	}
	
#### Problem

	{
		"ordinal": String,
		"text": String,
		"beginDate": YYYYMMDD,
		"active": boolean,
		"consultations": JsonArray
		
	}